﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacebookClone.Models.FaceNETModels;
using FacebookClone.Models.Utils;
using Microsoft.AspNet.Identity;

namespace FacebookClone.Controllers
{
    public class HomeController : Controller
    {
        FaceNETDbContext db = new FaceNETDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUsersList(string q)
        {
            string UserId = User.Identity.GetUserId();
            List<UserProfile> UserProfiles = db.UserProfiles.Where(x => x.IsPublic == true && x.UserId != UserId)
                                                            .OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ToList();
            
            var selectList = new List<Select2Model>(); 

            foreach(var userProfile in UserProfiles )
            {
                string selectText = userProfile.FirstName + ' ' + userProfile.LastName;
                string selectId = userProfile.UserId;

                selectList.Add(new Select2Model(selectId, selectText));
            }

            if (!(string.IsNullOrEmpty(q) || string.IsNullOrWhiteSpace(q)))
            {
                selectList = selectList.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }

            return Json(new { items = selectList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RedirectToUserProfile(string UserId)
        {
            return Json(new { result = "Redirect", url = Url.Action("Details/" + UserId, "UserProfiles") }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetMessages(string ReceiverId)
        {
            UserProfile newUserProfile;
            string UserId = User.Identity.GetUserId();
            List<Message> Messages = db.Messages.Where(x =>
                 (x.SenderId == UserId && x.ReceiverId == ReceiverId) || (x.ReceiverId == UserId && x.SenderId == ReceiverId)
            ).OrderByDescending(x => x.SendDateTime).Take(10).ToList();

            var result = new LinkedList<object>();

            foreach (var message in Messages)
            {
                newUserProfile = db.UserProfiles.Find(message.SenderId);
                result.AddLast(new
                {
                    Username = newUserProfile.FullName(),
                    SendDateTime = message.SendDateTime.ToString(),
                    MessageBody = message.MessageBody
                });
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddMessage(string ReceiverId, string MessageBody)
        {
            Message newMessage = new Message();
            newMessage.SenderId = User.Identity.GetUserId();

            newMessage.ReceiverId = ReceiverId;
            newMessage.MessageBody = MessageBody;
            newMessage.SendDateTime = DateTime.Now;

            db.Messages.Add(newMessage);
            db.SaveChanges();
            var result = new { message = "Success" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
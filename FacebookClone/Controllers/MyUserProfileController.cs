﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FacebookClone.Models.FaceNETModels;
using Microsoft.AspNet.Identity;

namespace FacebookClone.Controllers
{
    public class MyUserProfileController : Controller
    {
        private FaceNETDbContext db = new FaceNETDbContext();

        // GET: MyProfile
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                // TO DO 
                // redirect to login page
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var UserId = User.Identity.GetUserId();
            UserProfile MyUserProfile = db.UserProfiles.Find(UserId);

            if (MyUserProfile == null)
            {
                return HttpNotFound();
            }
            return View(MyUserProfile);
        }

        // GET: MyUserProfile/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userProfile = db.UserProfiles.Find(id);
            if (userProfile == null)
            {
                return HttpNotFound();
            }
            return View(userProfile);
        }

        // GET: MyUserProfile/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userProfile = db.UserProfiles.Find(id);
            if (userProfile == null)
            {
                return HttpNotFound();
            }
            return View(userProfile);
        }

        // POST: MyUserProfile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId,FirstName,LastName,SexId,IsPublic, ImagePath")] UserProfile userProfile)
        {
            if (ModelState.IsValid)
            {
                userProfile.ImagePath = "../../Images/" + userProfile.ImagePath;
                db.Entry(userProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userProfile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult AddPhotoAlbum(string userId)
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddPhotoAlbum(PhotoAlbum photoAlbum)
        {
            photoAlbum.UserProfileId = User.Identity.GetUserId();

            db.PhotoAlbums.Add(photoAlbum);

            db.SaveChanges();

            return View();
        }



        public ActionResult AddPhotoToAlbum(int albumId)
        {
            var photoModel = new Photo();
            photoModel.PhotoAlbumId = albumId;

            return View(photoModel);
        }


        [HttpPost]
        public ActionResult AddPhotoToAlbum(Photo photo)
        {

            photo.Path = "../../Images/" + photo.Path;

            db.Photos.Add(photo);

            db.SaveChanges();

            return View();
        }

        public ActionResult DeletePhotoAlbum(int albumId)
        {
            var photosToBeDeleted = db.Photos.Where(c => c.PhotoAlbumId == albumId).ToList();

            db.Photos.RemoveRange(photosToBeDeleted);

            var photoAlbumToBeDeleted = db.PhotoAlbums.FirstOrDefault(c => c.AlbumId == albumId);

            db.PhotoAlbums.Remove(photoAlbumToBeDeleted);

            db.SaveChanges();


            if (User.IsInRole("Administrator"))
            {
                return RedirectToAction("Index", "UserProfiles", new { area = "" });
            }

            return RedirectToAction("Index");
        }


        public ActionResult ConfirmRequest(int id)
        {
            var friendReq = db.FriendRequests.First(c => c.FriendRequestId == id);
            friendReq.IsAccepted = true;
            db.SaveChanges();

            return RedirectToAction("Index");
        }


    }
}

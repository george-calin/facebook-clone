﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FacebookClone.Models.FaceNETModels;

namespace FacebookClone.Controllers
{
    public class PhotosController : Controller
    {
        private FaceNETDbContext db = new FaceNETDbContext();

        // GET: Photos/5
        public ActionResult Gallery(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            List<Photo> photos = db.Photos.Where(x => x.PhotoAlbumId == Id).ToList();

            return View(photos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

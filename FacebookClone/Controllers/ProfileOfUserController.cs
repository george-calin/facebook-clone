﻿using FacebookClone.Models.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacebookClone.Controllers
{
    public class ProfileOfUserController : Controller
    {
        // GET: ProfileOfUser
        public ActionResult Index(string userId)
        {
            var userProfile = ProfileOfUserHelper.GetProfileOfUserByUserId(userId);
            return View();
        }
    }
}
﻿using FacebookClone.Models.FaceNETModels;
using FacebookClone.Models.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.BusinessLayer
{
    public static class ProfileOfUserHelper
    {
        private static FaceNETDbContext _ctx = new FaceNETDbContext();

        public static ProfileOfUserDTO GetProfileOfUserByUserId(string userId)
        {
            var profileUser = new ProfileOfUserDTO();

            profileUser.UserProfile = _ctx.UserProfiles.FirstOrDefault(x => x.UserId == userId);
            profileUser.PhotoAlbums = _ctx.PhotoAlbums.Where(x => x.UserProfileId == userId).ToList();

            return profileUser;

        }



        public static bool UsersAreFriends(string userId1, string userId2)
        {
            var friendReq = _ctx.FriendRequests.Where(c => c.IsAccepted && 
                   ((c.RequestedId == userId1 && c.RequesterId == userId2) ||
                    (c.RequestedId == userId1 && c.RequesterId == userId2)));

            if (friendReq.Any())
                return true;

            return false;
        }


        public static bool RequestIsSent(string userId1, string userId2)
        {
            var friendReq = _ctx.FriendRequests.Where(c => !c.IsAccepted &&
                   ((c.RequestedId == userId1 && c.RequesterId == userId2) ||
                    (c.RequestedId == userId2 && c.RequesterId == userId1))).ToList();

            if (friendReq.Any())
                return true;

            return false;
        }

        public static List<UserProfile> GetFriendsOfUserByUserId(string userId)
        {
            var friendReqs1 = _ctx.FriendRequests.Where(c => c.IsAccepted && c.RequestedId == userId).Select(c => c.RequesterId).ToList();
            var friendReqs2 = _ctx.FriendRequests.Where(c => c.IsAccepted && c.RequesterId == userId).Select(c => c.RequestedId).ToList();

            var friends = _ctx.UserProfiles.Where(c => friendReqs1.Contains(c.UserId) || friendReqs2.Contains(c.UserId));

            return friends.ToList();


        }

        public static List<FriendRequest> GetFriendRequestsByUserId(string userId)
        {
            return _ctx.FriendRequests.Where(c => !c.IsAccepted && c.RequestedId == userId).ToList();
        }

        
    }
}
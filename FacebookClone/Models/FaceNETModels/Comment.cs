﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        public string Text { get; set; }
        public DateTime? LeftOn { get; set; }

        [ForeignKey("Photo")]
        public int PhotoId { get; set; }
        public virtual Photo Photo { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class FriendRequest
    {
        [Key]
        public int FriendRequestId { get; set; }

        public bool IsAccepted { get; set; }
        public DateTime? AcceptedOn { get; set; }
        public string RequesterId { get; set; }
        public string RequestedId { get; set; }



    }
}
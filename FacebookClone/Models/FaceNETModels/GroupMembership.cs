﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class GroupMembership
    {
        [Key]
        public int GroupMembershipId { get; set; }
        public int GroupId { get; set; }
        public string UserId { get; set; }
    }
}
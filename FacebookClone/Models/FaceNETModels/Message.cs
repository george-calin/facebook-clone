﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class Message
    {
        [Key]
        public int MessageId { get; set; }

        public string SenderId { get; set; }
        public string ReceiverId { get; set; }

        public string MessageBody { get; set; }
        public DateTime SendDateTime { get; set; }
    }
}
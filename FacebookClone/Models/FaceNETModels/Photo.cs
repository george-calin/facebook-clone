﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class Photo
    {
        [Key]
        public int PhotoId { get; set; }
        
        public string Path { get; set; }
        public string Description { get; set; }

        [ForeignKey("PhotoAlbum")]
        public int PhotoAlbumId { get; set; }
        public virtual PhotoAlbum PhotoAlbum { get; set; }


        public virtual ICollection<Comment> Comments { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class PhotoAlbum
    {
        [Key]
        public int AlbumId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Photo> Photos { get; set; }


        [ForeignKey("UserProfile")]
        public string UserProfileId { get; set; }

        public virtual UserProfile UserProfile { get; set; }




    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.FaceNETModels
{
    public class UserProfile
    {
        public UserProfile()
        {
            UserId = "-1";
            FirstName = "pastaie";
            LastName = "gigi";
            SexId = 0;
            IsPublic = true;
            ImagePath = "../../Images/DefaultMan.jpg";
        }

        public UserProfile(string userId, string firstName, string lastName, int sexId, bool isPublic, string imagePath)
        {
            UserId = userId;
            FirstName = firstName;
            LastName = lastName;
            SexId = sexId;
            IsPublic = isPublic;

            if (imagePath == null)
            { 
                if (SexId == 1)
                    ImagePath = "../../Images/DefaultMan.jpg";
                else
                    ImagePath = "../../Images/DefaultWoman.jpg";
            }
            else
            {
                ImagePath = imagePath;
            }
        }

        public string FullName()
        {
            return FirstName + " " + LastName;
        }

        [Key]
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int SexId { get; set; }
        public bool IsPublic { get; set; }
        public string ImagePath { get; set; }
        
        public virtual ICollection<PhotoAlbum> PhotoAlbums { get; set;  } 
    }

}
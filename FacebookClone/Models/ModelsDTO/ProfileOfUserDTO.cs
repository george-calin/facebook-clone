﻿using FacebookClone.Models.FaceNETModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.ModelsDTO
{
    public class ProfileOfUserDTO
    {
        public UserProfile UserProfile { get; set; }
        public List<PhotoAlbum> PhotoAlbums { get; set; }
    }
}
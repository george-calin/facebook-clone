﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FacebookClone.Models
{
    public class UserProfileDTO
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Email-ul este obligatoriu")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Username-ul este obligatoriu")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Numele este obligatoriu")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Prenumele este obligatoriu")]
        public string LastName { get; set; }

        public bool IsPublic { get; set; }

        [Required(ErrorMessage = "Selectati sexul")]
        public int Sex { get; set; }

    }
}
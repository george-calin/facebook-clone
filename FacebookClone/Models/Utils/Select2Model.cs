﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacebookClone.Models.Utils
{
    public class Select2Model
    {

        public Select2Model(string selectId, string selectText)
        {
            this.id = selectId;
            this.text = selectText;
        }

        public string id { get; set; }
        public string text { get; set; }
    }
}
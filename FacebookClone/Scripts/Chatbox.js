﻿$(document).ready(function () {
    handleSelectsChatbox();
    handleChatBox();
    handleSendOnEnter();
});

var handleSelectsChatbox = function () {
    $('#select_user-chat').select2({
        placeholder: "Start Chatting",
        ajax: {
            url: '/Home/GetUsersList',
            width: 'resolve',
            data: function (params) {
                return {
                    q: params.term// search term
                };
            },
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
            minimumInputLength: 2,
        }
    })
    .on("change", updateChatArea);
}

var handleChatBox = function () {
    $("#message").html("Waiting for update...");
    var refreshId = setInterval("updateChatArea()", 5000);
}

var updateChatArea = function () {
    $.ajax({
        type: "POST",
        url: '/Home/GetMessages',
        data: { ReceiverId: $('#select_user-chat').val()},
        dataType: "json",
        success:  successFuncChatbox,
        error: errorFuncChatbox
    });
}

var successFuncChatbox = function (data) {
    $("#message").html += "Fetching...",
    $("#chatarea").html("");
    var x;
    if(data.length > 0) {
        for(x in data) {
            $("#chatarea").html(
            "<p><b>" +
            data[x].Username + "</b> <i>(" +
            data[x].SendDateTime + ")</i> - " +
            data[x].MessageBody + "</p>" +
            $("#chatarea").html()); 
        }
    }
    else {
        $("#chatarea").html("<p>No Messages</p>");
    }
    $("#message").html("Messages loaded.");
}

var errorFuncChatbox = function(response) {
    $("#message").html("Error");
}

function sendNewMessage() {
    $.ajax({
        type: "POST",
        url: "/Home/AddMessage",
        data: { ReceiverId: $('#select_user-chat').val(),
                MessageBody: $("#newmessage_messagebody").val()   
        },
        success: function(data) {
            updateChatArea();
            console.log(data);
        },
        error: function(result) {
            console.log(result);
        }    
    });

    $("#newmessage_messagebody").val("");
    updateChatArea();
}

function handleSendOnEnter() {
    $("#newmessage_messagebody").on('keyup', function (e) {
        if (e.keyCode == 13 && $("#newmessage_messagebody").val() != '' && $('#select_user-chat').val() != null)  {
            sendNewMessage();
        }
    });
    
}
﻿$(document).ready(function () {
    handleSelects();
});

var handleSelects = function () {
    $('#select_user-profiles').select2({
        placeholder: "Find your friends",
        ajax: {
            url: '/Home/GetUsersList',
            width: 'resolve',
            data: function (params) {
                return {
                    q: params.term// search term
                };
            },
            processResults: function (data) {
                return {
                    results: data.items
                };
            },
            minimumInputLength: 2,
        }
    })
    .on("change", function (e) {
        
        var userId = $("#select_user-profiles").val();

        $.ajax({
            type: "GET",
            url: '/Home/RedirectToUserProfile',
            data: {UserId : userId}, 
            dataType: "json",
            success: successFunc,
            error: errorFunc
        });
        
    });    
}

var successFunc = function (response) {
    if(response.result == 'Redirect')
        window.location = response.url;
    // alert("E Bine, da-i inainte");
}
var errorFunc = function(response) {
    alert("Nu e binee, nu-i da inaintee");
}